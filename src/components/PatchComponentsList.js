import React, { Component, Fragment } from "react";
import PatchComponent from './PatchComponent.js';


class PatchComponentsList extends Component {
    render() {
        const { patch, type } = this.props;
        return <Fragment>{Array.from(Array(patch), (e, i) => {
            return <PatchComponent stepNumber={i} updatePatchParam={this.props.updatePatchParam} deletePatchParam={this.props.deletePatchParam} />
        })}</Fragment>
    }
}

export default PatchComponentsList;
