import Table from "react-bootstrap/Table";
import React, { Component, Fragment } from "react";


class PatchTypeSelector extends Component {

    handleChange = (event) => {
        this.props.updatePatchType(event.target.value);
    }

    render() {
        return <Fragment>
            <label>
                Type:
                <select onChange={this.handleChange}>
                    <option selected value="sql">SQL</option>
                    <option value="webservice">Web Service</option>
                </select>
            </label>
        </Fragment>
    }
}

export default PatchTypeSelector;
