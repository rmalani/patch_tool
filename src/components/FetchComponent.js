import Table from "react-bootstrap/Table";
import React, { Component, Fragment } from "react";
import { Button } from 'react-bootstrap';

class FetchComponent extends Component {

    state = {
        api: '',
        assetIds: [],
        checkAttribute: '',
        isSaved: false,
        sql: '',
        idColumn: '',
    };

    toggleFetchParams = (e) => {
        e.preventDefault();
        const {
            service,
            type,
        } = this.props;
        const {
            api,
            assetIds,
            checkAttribute,
            isSaved,
            sql,
            idColumn,
        } = this.state;
        const checkAttributeObject = checkAttribute ? { checkAttribute: checkAttribute } : {};
        const fetchParam = type === 'webservice' ? {
            service,
            type,
            api,
            assetIds: assetIds.split(','),
            action: 'fetch',
            ...checkAttributeObject,
        } : {
            service,
            type,
            sql,
            idColumn,
            action: 'fetch',
            ...checkAttributeObject,
        }
        if(!isSaved) {
            this.props.updateFetchParam(fetchParam);
        } else {
            this.props.deleteFetchParam();
        }
        this.setState({ isSaved: !isSaved });
    }

    updateApi = (e) => {
        this.setState({ api: e.target.value });
    }

    updateAssetIds = (e) => {
        this.setState({ assetIds: e.target.value });
    }

    updateCheckAttribute = (e) => {
        this.setState({ checkAttribute: e.target.value });
    }

    updateSql = (e) => {
        this.setState({ sql: e.target.value });
    }

    updateIdColumn = (e) => {
        this.setState({ idColumn: e.target.value });
    }

    render() {
        const { type } = this.props;
        const { isSaved } = this.state;
        return <Fragment>
            <h3>Fetch</h3>
            <Table striped bordered hover variant="dark">
                <tbody>
                { type === 'webservice' && !isSaved && <tr>
                    <td><input onChange={this.updateApi} type="text" name="api" placeholder='Enter the api' /></td>
                    <td><input onChange={this.updateAssetIds} type="text" name="assetIds" placeholder='Enter the asset ids' /></td>
                    <td><input onChange={this.updateCheckAttribute} type="text" name="checkAttribute" placeholder='Check attributes' /></td>
                </tr> }
                { type === 'webservice' && isSaved && <tr>
                    <td>Api: {this.state.api}</td>
                    <td>Id: {this.state.id}</td>
                    <td>Check Attribute: {this.state.checkAttribute}</td>
                </tr> }
                { type === 'sql' && !isSaved && <tr>
                    <td><input onChange={this.updateSql} type="text" name="sql" placeholder='Enter SQL' /></td>
                    <td><input onChange={this.updateIdColumn} type="text" name="idColumn" placeholder='Enter the id column' /></td>
                    <td><input onChange={this.updateCheckAttribute} type="text" name="checkAttribute" placeholder='Check attributes' /></td>
                </tr> }
                { type === 'sql' && isSaved && <tr>
                    <td>SQL: {this.state.sql}</td>
                    <td>Id Column: {this.state.idColumn}</td>
                    <td>Check Attribute: {this.state.checkAttribute}</td>
                </tr> }
                </tbody>
            </Table>
            <Button variant="success" size='sm' onClick={this.toggleFetchParams}>{ !isSaved ? 'Save' : 'Edit' }</Button>
        </Fragment>
    }
}

export default FetchComponent;
