import React, { Component, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table'
import FetchComponent from './FetchComponent.js';
import PatchComponentsList from './PatchComponentsList.js';
import ServiceSelector from './ServiceSelector.js';
import PatchTypeSelector from './PatchTypeSelector.js';
import ReactJsonSyntaxHighlighter from 'react-json-syntax-highlighter'
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';



class App extends React.Component {

    state = {
        fetch: false,
        patch: 0,
        service: 'provisioningservice',
        patchType: 'sql',
        api: '',
        id: '',
        checkAttribute: '',
        requestArray: [],
        isDone: false,
    };

    generateJSON = (e) => {
        const {
            requestArray,
        } = this.state;
        e.preventDefault();
        this.setState({ isDone: true });
    }

    addFetchStep = () => {
        this.setState({
            fetch: true,
        });
    }

    addPatchStep = () => {
        const { patch } = this.state;
        this.setState({
            patch: patch+1,
        });
    }

    updateService = (service) => {
        this.setState({ service });
    }

    updatePatchType = (patchType) => {
        this.setState({ patchType });
    }

    updateFetchParam = (fetchParam) => {
        const { requestArray } = this.state;
        const newRequestArray = requestArray.concat(fetchParam);
        this.setState({ requestArray: newRequestArray });
    }

    deleteFetchParam = () => {
        //TODO function for deleting fetch param from request Array
        console.log('Delete fetch param from request array');
        const {
            requestArray,
        } = this.state;
        const newRequestArray = requestArray.filter((item) => item.action !== 'fetch');
        this.setState({
            requestArray: newRequestArray,
        });
    }

    updatePatchParam = (patchParam) => {
        const { requestArray } = this.state;
        const newRequestArray = requestArray.concat(patchParam);
        this.setState({ requestArray: newRequestArray });
    }

    deletePatchParam = () => {
        //TODO function for deleting fetch param from request Array
        console.log('Delete last patch param from request array');
        const {
            requestArray,
        } = this.state;
        const newRequestArray = requestArray.filter((item) => item.action !== 'patch');
        this.setState({
            requestArray: newRequestArray,
        });
    }

    copyToClipboard = (e) => {
        e.preventDefault();
        const { requestArray } = this.state;
        const str = JSON.stringify(requestArray, null, 2);
        const el = document.createElement('textarea');  // Create a <textarea> element
        el.value = str;                                 // Set its value to the string that you want copied
        el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
        el.style.position = 'absolute';
        el.style.left = '-9999px';                      // Move outside the screen to make it invisible
        document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
        const selected =
            document.getSelection().rangeCount > 0        // Check if there is any content selected previously
                ? document.getSelection().getRangeAt(0)     // Store selection if found
                : false;                                    // Mark as false to know no selection existed before
        el.select();                                    // Select the <textarea> content
        document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
        document.body.removeChild(el);                  // Remove the <textarea> element
        if (selected) {                                 // If a selection existed before copying
            document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
            document.getSelection().addRange(selected);   // Restore the original selection
        }
    };


    render() {
        const {
            fetch,
            patch,
            patchType,
            isDone,
            requestArray,
        } = this.state;
        if(!isDone) {
            return (
                <Fragment>
                    <div style={ { height: '800px', width: '800px', marginLeft: '200px', marginTop: '50px' } }>
                        <h1> Java Based Patch Tool </h1>
                        <table border='1px' style={{ marginTop: '50px', marginBottom: '50px' }}>
                            <tr colSpan={2}>
                                <ServiceSelector updateService={this.updateService} />
                                <PatchTypeSelector updatePatchType={this.updatePatchType} />
                            </tr>
                            <tr>{ fetch && <FetchComponent service={this.state.service} type={patchType} updateFetchParam={this.updateFetchParam} deleteFetchParam={this.deleteFetchParam}/> } </tr>
                            <tr>{ patch > 0 && <PatchComponentsList updatePatchParam={this.updatePatchParam} deletePatchParam={this.deletePatchParam} patch={patch} /> } </tr>
                        </table>
                        { !fetch && <Button size='sm' style={{ marginRight: '5px' }} onClick={ this.addFetchStep } >Add Fetch Step</Button> }
                        { fetch && <Button size='sm' style={{ marginRight: '5px' }} onClick={ this.addPatchStep }>Add Patch Step</Button> }
                        { patch > 0 && <Button size='sm' onClick={ this.generateJSON }>Generate</Button> }
                    </div>
                </Fragment>
            );
        }
        return (
            <Fragment>
                <div style={ { height: '800px', width: '800px', marginLeft: '200px', marginTop: '50px' } }>
                    <h1> Java Based Patch Tool </h1>
                    <SyntaxHighlighter language="json" style={docco}>
                        {(JSON.stringify(requestArray, null, 2))}
                    </SyntaxHighlighter>
                    <Button onClick={this.copyToClipboard} variant="info">Copy JSON to Clipboard</Button>
                </div>
            </Fragment>
        );
    }
};

export default App;
