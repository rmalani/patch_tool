import Table from "react-bootstrap/Table";
import React, { Component, Fragment } from "react";
import { Button } from 'react-bootstrap';

class PatchComponent extends Component {

    state = {
        api: '',
        checkAttribute: '',
        isSaved: false,
    };

    togglePatchParams = (e) => {
        e.preventDefault();
        const {
            api,
            checkAttribute,
            isSaved,
        } = this.state;
        if(!isSaved) {
            const checkAttributeObject = checkAttribute ? { checkAttribute: checkAttribute } : {};
            const patchParam = {
                api,
                action: 'patch',
                ...checkAttributeObject,
            };
            this.props.updatePatchParam(patchParam);
        } else {
            this.props.deletePatchParam();
        }
        this.setState({ isSaved: !isSaved });
    }

    updateApi = (e) => {
        this.setState({ api: e.target.value });
    }

    updateCheckAttribute = (e) => {
        this.setState({ checkAttribute: e.target.value });
    }

    render() {
        const { stepNumber } = this.props;
        const { isSaved } = this.state;
        return <Fragment>
            <h3>Patch Step {stepNumber+1}</h3>
            <Table striped bordered hover variant="dark">
                <tbody>
                { !isSaved && <tr>
                    <td><input onChange={this.updateApi} type="text" name="api" placeholder='Enter the api' /></td>
                    <td><input onChange={this.updateCheckAttribute} type="text" name="checkAttribute" placeholder='Check attributes' /></td>
                </tr> }
                { isSaved && <tr>
                    <td>Api: { this.state.api }</td>
                    <td>Check Attribute: { this.state.checkAttribute }</td>
                </tr> }
                </tbody>
            </Table>
            <Button variant="success" size='sm' onClick={this.togglePatchParams}>{ !isSaved ? 'Save' : 'Edit' }</Button>
        </Fragment>
    }
}

export default PatchComponent;
