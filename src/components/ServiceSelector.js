import Table from "react-bootstrap/Table";
import React, { Component, Fragment } from "react";


class ServiceSelector extends Component {

    handleChange = (event) => {
        this.props.updateService(event.target.value);
    }

    render() {
        return <Fragment>
            <label>
                Service:
                <select onChange={this.handleChange}>
                    <option selected value="provisioningservice">Provisioning Service</option>
                    <option value="mlm">MLM</option>
                    <option value="calendar">Calendar</option>
                    <option value="treeservice">Tree Service</option>
                    <option value="assetapi">Asset API</option>
                </select>
            </label>
        </Fragment>
    }
}

export default ServiceSelector;
